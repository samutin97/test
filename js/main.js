import "../style/main.scss";

const menu = document.querySelector(".header__menu");
menu.contentHeight = menu.getBoundingClientRect().height;
menu.style.height = "0px";
menu.toggleMenu = function () {
  this.style.height = this.style.height === "0px"
    ? menu.contentHeight + "px"
    : "0px";
}

document.querySelector(".fa-bars").addEventListener("click", () => {
  menu.toggleMenu();
})

const linksAndImages = [...document.getElementsByTagName("a"), ...document.getElementsByTagName("img")];
linksAndImages.forEach((elem) => {
  elem.addEventListener("dragstart", (e) => { e.preventDefault(); });
});

const sliders = [...document.getElementsByClassName("slider")];
sliders.forEach((elem) => {
  let mouseDownPosition = null;
  let isMove = false;
  const rightLimiter = elem.getBoundingClientRect().right - elem.children.item(elem.childElementCount-1).getBoundingClientRect().right;

  const moveFunc = (e) => {
    if (e.clientX < mouseDownPosition + 3 && e.clientX > mouseDownPosition - 3) return;
    e.preventDefault();
    e.stopPropagation();
    elem.style.left = (parseInt(elem.style.left || 0) + e.clientX - mouseDownPosition) + "px";
    mouseDownPosition = e.clientX;
    isMove = true;
    if (parseInt(elem.style.left || 0) > 0) elem.style.left = "0px";
    if (parseInt(elem.style.left || 0) < rightLimiter) elem.style.left = rightLimiter + "px";
  };

  elem.addEventListener("mousedown", (e) => {
    mouseDownPosition = e.clientX
    document.addEventListener("mousemove", moveFunc,{capture: true})
  });

  document.addEventListener("click", (e) => {
    if(isMove) {
      e.stopPropagation();
      e.preventDefault();
      isMove = false;
    }
    document.removeEventListener("mousemove", moveFunc, {capture: true});
  });
})

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CssWebpackPlugin = require('mini-css-extract-plugin');

module.exports = [
  {
    name: "develop",
    devtool: "eval-source-map",
    entry: './js/main.js',
    module: {
      rules: [
        {
          test: /\.(png|woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            CssWebpackPlugin.loader,
            'css-loader',
            "sass-loader"
          ]
        },
        {
          test: /\.svg$/,
          type: 'asset/resource',
          use: 'svgo-loader'
        },
        {
          test: /\.html$/i,
          loader: "html-loader",
        }
      ]
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'main.js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./index.html"
      }),
      new CssWebpackPlugin({
        filename: "style.css"
      })
    ],
    devServer: {
      client: {
        progress: true,
      },
    },
    mode: 'development'
  },
  {
    name: "product",
    entry: './js/main.js',
    module: {
      rules: [
        {
          test: /\.(png|woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            CssWebpackPlugin.loader,
            'css-loader',
            "sass-loader"
          ]
        },
        {
          test: /\.svg$/,
          type: 'asset/resource',
          use: 'svgo-loader'
        },
        {
          test: /\.html$/i,
          loader: "html-loader",
        }
      ]
    },
    optimization: {
      minimize: false
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'main.js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./index.html",
        minify: false
      }),
      new CssWebpackPlugin({
        filename: "style.css"
      })
    ],
    mode: 'production'
  }
]
